var gulp = require('gulp'),
    stylus = require('gulp-stylus'),
    prefix = require('gulp-autoprefixer'),
    browserSync = require('browser-sync'),
    postcss = require('gulp-postcss'),
    assets = require('postcss-assets'),
    concat = require('gulp-concat'),
    plumber = require('gulp-plumber'),
    jeet = require('jeet'),
    rupture = require('rupture'),
    sourcemaps = require('gulp-sourcemaps'),
    poststylus = require('poststylus'),
    autoprefixer = require('autoprefixer-core'),
    rucksack = require('rucksack-css'),
    cssnano = require('cssnano'),
    cache = require('gulp-cached'),
    progeny = require('gulp-progeny'),
    center = require('postcss-center'),
    fontpack = require('postcss-font-pack'),
    watch = require('gulp-watch'),
    path = require('path'),
    remember = require('gulp-remember'),
    reload = browserSync.reload;

// Собираем Stylus
gulp.task('stylus', function() {
    var processors = [
        assets({
            loadPaths: ['assets/icons/']

        }),
        'postcss-clearfix',
        'postcss-short',
        'postcss-easings',
        'postcss-default-unit',
        'postcss-merge-longhand',
        'lost',
        // 'postcss-discard-comments',
        'postcss-center',
        'autoprefixer-core',
        'cssnano'
    ];


        gulp.src(['./assets/b/**/*.styl', '!./assets/b/**/_*.styl'])
            //.pipe(watch(['./assets/b/**/*.styl', '!./assets/b/**/_*.styl']))
            // .pipe(plumber())
            // .pipe(sourcemaps.init())
            // .pipe(cache('style'))
            // .pipe(progeny())
            .pipe(stylus({
                import: ['jeet', 'rupture', path.resolve(__dirname) + '/../index.styl'],
                use: [
                    jeet(),
                    rupture(),
                    poststylus([

                        assets({
                            loadPaths: ['assets/icons/']
                        }),
                        'postcss-clearfix',
                        'postcss-short',
                        'postcss-easings',
                        // 'postcss-default-unit',
                        'postcss-merge-longhand',
                        'postcss-discard-comments',
                        'postcss-center',
                        'postcss-triangle',
                        'lost',
                        'autoprefixer-core'

                        //'cssnano'

                    ])
                ]

            }))
            // .pipe(postcss(processors))
            .pipe(concat('blocks.css'))
            //.pipe(sourcemaps.write())
            .pipe(gulp.dest('./public/css/')) // записываем css
            .pipe(browserSync.stream());
});
