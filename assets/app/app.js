var SVG = require('./svg.js')

function drawButton() {
    Array.prototype.slice.call(document.querySelectorAll('.buttonGradient')).
    forEach(function(button, i) {
        var buttonHeight = button.offsetHeight
        var buttonWidth = button.offsetWidth
        var draw = SVG(button).size(buttonWidth, buttonHeight).viewbox(0, 0, buttonWidth, buttonHeight).spof()
        var gradient = draw.gradient('linear', function(stop) {
            stop.at(0, '#75b94c')
            stop.at(1, '#03b2d7')
        })
        gradient.from(0, 0).to(1, 1)
        var react = draw.rect(buttonWidth - 4, buttonHeight - 4).attr({
            'stroke-width': '2',
            'stroke': gradient,
            'fill': gradient,
            'fill-opacity': 0,
            x: 2,
            y: 2
        }).radius(20)
        button.addEventListener('mouseover', function() {
            react.animate(300).attr({
                    'fill-opacity': 1
                })
                //.attr({ 'fill-opacity': 1 })
        })
        button.addEventListener('mouseout', function() {
            react.animate(300).attr({
                'fill-opacity': 0
            })
        })

        //SVG.on(window, 'resize', function() { draw.spof() })
        window.onload = window.onresize = function() {
            draw.spof()
        }

    })
}

document.addEventListener("DOMContentLoaded", function() {
    drawButton()
});
